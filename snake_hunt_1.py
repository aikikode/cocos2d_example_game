#!/usr/bin/env python
#
# Add empty window

from cocos.layer import Layer
from cocos.scene import Scene
from cocos.director import director


class Game(Layer):
    pass

director.init(resizable=False, width=800, height=600, caption="Snake Hunt")
scene = Scene()
scene.add(Game(), z=0, name="game layer")
director.run(scene)
