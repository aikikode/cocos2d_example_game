#!/usr/bin/env python
#
# Add boundaries

import pyglet
import cocos
from cocos.layer import Layer
from cocos.scene import Scene
from cocos.director import director
from pyglet.window import key
from cocos.actions import MoveBy

# Add images to our game
IMAGES_RESOURCE = "data/img"
pyglet.resource.path = [IMAGES_RESOURCE]
pyglet.resource.reindex()

config = {
    'step': 8,
}


class Game(Layer):
    is_event_handler = True

    def __init__(self):
        super(Game, self).__init__()
        self.width, self.height = director.get_window_size()
        # Background image
        bg = cocos.sprite.Sprite('bg.png')
        bg.position = (self.width / 2, self.height / 2)
        self.add(bg, z=0)
        # Snake
        self.snake = cocos.sprite.Sprite('snake.png')
        self.snake.scale = 0.3
        self.snake.position = (self.width / 2, self.snake.height / 2)
        self.add(self.snake, z=1)
        # Home
        self.home = cocos.sprite.Sprite('tree.png')
        self.home.scale = 0.3
        self.home.position = (self.width / 2, self.height - self.home.height / 2)
        self.add(self.home, z=1)
        # Add boundaries
        self.left_border = self.snake.width / 2
        self.right_border = self.width - self.snake.width / 2
        self.bottom_border = self.snake.height / 2
        self.top_border = self.height - self.snake.height / 2
        #
        self.keys = set()
        self.schedule_interval(self.update, 1/60.)

    def on_key_press(self, k, m):
        self.keys.add(k)
        return False

    def on_key_release(self, k, m):
        self.keys.remove(k)
        return False

    def update(self, dt):
        x, y = self.snake.position
        x = self.left_border if x < self.left_border else x
        x = self.right_border if x > self.right_border else x
        y = self.bottom_border if y < self.bottom_border else y
        y = self.top_border if y > self.top_border else y
        self.snake.position = (x, y)

        step = config['step']
        move = (0, 0)
        if key.LEFT in self.keys and x > self.left_border:
            move = tuple(sum(t) for t in zip(move, (-step, 0)))
        if key.RIGHT in self.keys and x < self.right_border:
            move = tuple(sum(t) for t in zip(move, (step, 0)))
        if key.UP in self.keys and y < self.top_border:
            move = tuple(sum(t) for t in zip(move, (0, step)))
        if key.DOWN in self.keys and y > self.bottom_border:
            move = tuple(sum(t) for t in zip(move, (0, -step)))
        self.snake.do(MoveBy(move, 0))

director.init(resizable=False, width=800, height=600, caption="Snake Hunt")
scene = Scene()
scene.add(Game(), z=0, name="game layer")
director.run(scene)
