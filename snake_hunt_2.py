#!/usr/bin/env python
#
# Add bg and snake

import pyglet
import cocos
from cocos.layer import Layer
from cocos.scene import Scene
from cocos.director import director

# Add images to our game
IMAGES_RESOURCE = "data/img"
pyglet.resource.path = [IMAGES_RESOURCE]
pyglet.resource.reindex()


class Game(Layer):
    def __init__(self):
        super(Game, self).__init__()
        self.width, self.height = director.get_window_size()
        # Background image
        bg = cocos.sprite.Sprite('bg.png')
        bg.position = (self.width / 2, self.height / 2)
        self.add(bg, z=0)
        # Snake
        self.snake = cocos.sprite.Sprite('snake.png')
        self.snake.scale = 0.3
        self.snake.position = (self.width / 2, self.snake.height / 2)
        self.add(self.snake, z=1)
        # Home
        self.home = cocos.sprite.Sprite('tree.png')
        self.home.scale = 0.3
        self.home.position = (self.width / 2, self.height - self.home.height / 2)
        self.add(self.home, z=1)

director.init(resizable=False, width=800, height=600, caption="Snake Hunt")
scene = Scene()
scene.add(Game(), z=0, name="game layer")
director.run(scene)
