#!/usr/bin/env python
#
# Add menu and end game screen

import cocos
from cocos.actions import MoveBy, JumpBy, Repeat, Reverse, RotateBy, Delay
import cocos.collision_model as cm
from cocos.director import director
from cocos.layer import Layer, ColorLayer
from cocos.menu import MenuItem, Menu, CENTER, shake, shake_back
from cocos.scene import Scene
from cocos.text import Label
import pyglet
from pyglet.window import key

# Add images to our game
IMAGES_RESOURCE = "data/img"
pyglet.resource.path = [IMAGES_RESOURCE]
pyglet.resource.reindex()

config = {
    'step': 8,
}


class Game(Layer):
    is_event_handler = True

    def __init__(self, end_game_layer=None):
        super(Game, self).__init__()
        self.end_game_layer = end_game_layer
        self.width, self.height = director.get_window_size()
        self.collision_manager = cm.CollisionManagerBruteForce()
        # Snake
        self.snake = cocos.sprite.Sprite('snake.png')
        self.snake.scale = 0.3
        self.snake.position = (self.width / 2, self.snake.height / 2)
        self.snake.cshape = cm.AARectShape(
            self.snake.position,
            self.snake.width / 2,
            self.snake.height / 2
        )
        self.add(self.snake, z=1)
        self.collision_manager.add(self.snake)
        # Home
        self.home = cocos.sprite.Sprite('tree.png')
        self.home.scale = 0.3
        self.home.position = (self.width / 2, self.height - self.home.height / 2)
        self.home.cshape = cm.AARectShape(
            self.home.position,
            self.home.width / 2,
            self.home.height / 2
        )
        self.add(self.home, z=1)
        self.collision_manager.add(self.home)
        # Add boundaries
        self.left_border = self.snake.width / 2
        self.right_border = self.width - self.snake.width / 2
        self.bottom_border = self.snake.height / 2
        self.top_border = self.height - self.snake.height / 2
        # Add enemies
        self.hunters = []
        self.add_hunter(50, self.height / 4., MoveBy((self.width / 2, 0), duration=1.8))
        self.add_hunter(self.width / 2, self.height / 4., JumpBy((self.width / 2, 0), 70, 3, duration=1.7))
        self.add_hunter(50, self.height * 5 / 9.,
                        JumpBy((self.width / 4, -50), 70, 1, duration=0.6) +
                        MoveBy((self.width / 4, 0), duration=1))
        self.add_hunter(self.width / 2, self.height * 5 / 9,
                        RotateBy(2 * 360, duration=1.9) |
                        JumpBy((self.width / 2, 0), 80, 2, duration=1.9))
        self.add_hunter(self.width * 2 / 3, self.height,
                        MoveBy((0, -self.height / 5), duration=0.4) +
                        MoveBy((-self.width / 6, 0), duration=0.4) +
                        Delay(0.4) +
                        MoveBy((-self.width / 6, 0), duration=0.4) +
                        MoveBy((0, self.height / 5), duration=0.4))
        for h in self.hunters:
            h.cshape = cm.AARectShape(
                h.position,
                h.width / 2,
                h.height / 2
            )
            self.add(h, z=1)
            self.collision_manager.add(h)
        #
        self.keys = set()
        self.game_ended = False
        self.schedule_interval(self.update, 1/60.)

    def add_hunter(self, x, y, action=None):
        hunter = cocos.sprite.Sprite('hunter.png')
        hunter.scale = 0.3
        hunter.position = (x, y)
        if action:
            hunter.do(Repeat(action + Reverse(action)))
        self.hunters.append(hunter)

    def on_key_press(self, k, m):
        self.keys.add(k)
        return False

    def on_key_release(self, k, m):
        try:
            self.keys.remove(k)
        except KeyError:
            pass
        return False

    def update(self, dt):
        if self.game_ended:
            return
        x, y = self.snake.position
        x = self.left_border if x < self.left_border else x
        x = self.right_border if x > self.right_border else x
        y = self.bottom_border if y < self.bottom_border else y
        y = self.top_border if y > self.top_border else y
        self.snake.position = (x, y)

        self.snake.cshape.center = self.snake.position
        for h in self.hunters:
            h.cshape.center = h.position

        collisions = self.collision_manager.objs_colliding(self.snake)
        if collisions:
            self.game_ended = True
            if self.home in collisions:
                label = "You got home!"
            else:
                label = "You lost!"
            if self.end_game_layer:
                self.end_game_layer.show_game_end_screen(text=label)

        step = config['step']
        move = (0, 0)
        if key.LEFT in self.keys and x > self.left_border:
            move = tuple(sum(t) for t in zip(move, (-step, 0)))
        if key.RIGHT in self.keys and x < self.right_border:
            move = tuple(sum(t) for t in zip(move, (step, 0)))
        if key.UP in self.keys and y < self.top_border:
            move = tuple(sum(t) for t in zip(move, (0, step)))
        if key.DOWN in self.keys and y > self.bottom_border:
            move = tuple(sum(t) for t in zip(move, (0, -step)))
        self.snake.do(MoveBy(move, 0))


class BackgroundLayer(Layer):
    def __init__(self):
        super(BackgroundLayer, self).__init__()
        self.width, self.height = director.get_window_size()
        # Background image
        bg = cocos.sprite.Sprite('bg.png')
        bg.position = (self.width / 2, self.height / 2)
        self.add(bg, z=0)


class MainMenu(Menu):
    def __init__(self):
        super(MainMenu, self).__init__('Snake Hunt')
        self.menu_anchor_x = CENTER
        self.menu_anchor_y = CENTER

        self.font_title['font_name'] = 'Edit Undo Line BRK'
        self.font_title['font_size'] = 72
        self.font_title['color'] = (50, 50, 50, 255)
        self.font_item['font_name'] = 'Edit Undo Line BRK',
        self.font_item['font_size'] = 46
        self.font_item['color'] = (50, 50, 50, 255)
        self.font_item_selected['font_name'] = 'Edit Undo Line BRK'
        self.font_item_selected['font_size'] = 56
        self.font_item_selected['color'] = (50, 50, 50, 255)

        items = [MenuItem('Start', self.on_start_game),
                 MenuItem('Quit', self.on_quit)]
        self.create_menu(items, shake(), shake_back())

    def on_start_game(self):
        scene = Scene()
        end_game = EndGame()
        scene.add(BackgroundLayer(), z=0)
        scene.add(Game(end_game), z=1, name="game layer")
        scene.add(end_game, z=2)
        director.push(scene)

    def on_quit(self):
        pyglet.app.exit()


class EndGame(Layer):
    def __init__(self):
        super(EndGame, self).__init__()
        self.background = ColorLayer(0, 0, 0, 170)
        self.width, self.height = director.get_window_size()

    def show_game_end_screen(self, text="Game Over"):
        self.add(self.background)
        self.add(Label(text,
                       (self.width / 2, self.height / 2),
                       font_name='Edit Undo Line BRK', font_size=32, anchor_x='center'))


def new_game():
    director.init(resizable=False, width=800, height=600, caption="Snake Hunt")
    scene = Scene()
    scene.add(BackgroundLayer(), z=0)
    scene.add(MainMenu(), z=1, name="main menu layer")
    director.run(scene)


if __name__ == '__main__':
    new_game()
